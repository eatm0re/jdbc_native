package edu.eatmore.jdbcnative;

import java.io.Serializable;
import java.util.Objects;

public class KsaSelling implements Serializable {

  private long id;
  private String description;
  private float amount;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public float getAmount() {
    return amount;
  }

  public void setAmount(float amount) {
    this.amount = amount;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    KsaSelling that = (KsaSelling) o;
    return id == that.id &&
        Float.compare(that.amount, amount) == 0 &&
        Objects.equals(description, that.description);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, description, amount);
  }

  @Override
  public String toString() {
    return "KsaSelling{" +
        "id=" + id +
        ", description='" + description + '\'' +
        ", amount=" + amount +
        '}';
  }
}
