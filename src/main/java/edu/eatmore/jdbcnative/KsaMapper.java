package edu.eatmore.jdbcnative;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class KsaMapper {

  private static final String KSA_SELLING_ID = "SELLING_ID";
  private static final String KSA_SELLING_DESCRIPTION = "SELLING_DESCRIPTION";
  private static final String KSA_SELLING_AMOUNT = "SELLING_AMOUNT";

  private KsaMapper() {
  }

  public static List<KsaSelling> mapKsaSelling(ResultSet rs) throws SQLException {
    List<KsaSelling> result = new ArrayList<>();
    while (rs.next()) {
      result.add(mapOneKsaSelling(rs));
    }
    return result;
  }

  public static KsaSelling mapOneKsaSelling(ResultSet rs) throws SQLException {
    KsaSelling result = new KsaSelling();
    result.setId(rs.getLong(KSA_SELLING_ID));
    result.setDescription(rs.getString(KSA_SELLING_DESCRIPTION));
    result.setAmount(rs.getFloat(KSA_SELLING_AMOUNT));
    return result;
  }
}
