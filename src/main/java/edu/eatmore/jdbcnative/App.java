package edu.eatmore.jdbcnative;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class App {

  public static void main(String[] args) throws SQLException {
    try (Connection connection = DriverManager.getConnection(
        PropertyFactory.getUrl(),
        PropertyFactory.getUsername(),
        PropertyFactory.getPassword())) {
      Statement statement = connection.createStatement();
      ResultSet rs = statement.executeQuery("select * from KSA$SELLING");
      List<KsaSelling> sellings = KsaMapper.mapKsaSelling(rs);
      System.out.println(sellings.toString());
    }
  }
}
