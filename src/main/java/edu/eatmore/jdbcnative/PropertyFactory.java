package edu.eatmore.jdbcnative;

import java.io.IOException;
import java.util.Properties;

public final class PropertyFactory {

  private static final Properties properties;

  static {
    properties = new Properties();
    try {
      properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("application.properties"));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private PropertyFactory() {
  }

  public static String getUrl() {
    return properties.getProperty("jdbcNative.url");
  }

  public static String getUsername() {
    return properties.getProperty("jdbcNative.username");
  }

  public static String getPassword() {
    return properties.getProperty("jdbcNative.password");
  }
}
